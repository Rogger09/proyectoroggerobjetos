package ObjetosRogger;

public class guerreroArma {
	
	protected String armaNombre;
	protected int armaDa�o;
	
	//Constructor
	
	protected guerreroArma (String armaNombre, int armaDa�o) {
		this.armaNombre = armaNombre;
		this.armaDa�o = armaDa�o;
	}
	
	@Override
	public String toString() {
		return "	Arma: " + main.espaciado(armaNombre, 15) + "	Da�o: " + armaDa�o;
	}

	//Setters & Getters
	public String getArmaNombre() {
		return armaNombre;
	}

	public void setArmaNombre(String armaNombre) {
		this.armaNombre = armaNombre;
	}

	public int getArmaDa�o() {
		return armaDa�o;
	}

	public void setArmaDa�o(int armaDa�o) {
		this.armaDa�o = armaDa�o;
	}
}
