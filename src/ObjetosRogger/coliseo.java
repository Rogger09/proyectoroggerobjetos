package ObjetosRogger;

import ObjetosRogger.clases.*;

public class coliseo {
	
	private guerrero[] guerreros;
	private int numGuerreros;
	private int topGuerreros;
	
	//Constructor
	coliseo() {
		guerreros = new guerrero[50];
		numGuerreros = 0;
		topGuerreros = 50;
	}
	
	coliseo(int dimensiones) {
		if (dimensiones < 1 || dimensiones > 50)
			dimensiones = 50;
		
		topGuerreros = dimensiones;
		guerreros = new guerrero[dimensiones];
	}
	
	//Metodos
	
	public int a�adirGuerrero (guerrero a�adir) {
		
		if (numGuerreros >= topGuerreros)
			return -1;

		guerreros[numGuerreros] = a�adir;
		numGuerreros++;

		return numGuerreros;
	}
	
	@Override
	public String toString() {
		return "Gossos: " + numGuerreros + "/" + topGuerreros;
	}
	
	public void visualizar() {
		for (int i = 0; i < numGuerreros; i++)
			System.out.println("- " + guerreros[i]);
	}
	
	public void visualizarVivos() {
		for (int i = 0; i < numGuerreros; i++)
			if (guerreros[i].getEstado() == guerreroEstado.VIVO)
				System.out.println("- " + guerreros[i]);
	}
	
	public void visualizarMorts() {
		for (int i = 0; i < numGuerreros; i++)
			if (guerreros[i].getEstado() == guerreroEstado.MUERTO)
				System.out.println("- " + guerreros[i]);
	}
	
	//GENERAR Coliseo
		public static coliseo generarColiseo(int max) {

			coliseo coli = new coliseo(max);
						
			// Generar nombres
			String[] nomGuerreros = {
				"Arturo", "Hercules", "Alejandro", "Emiya", "Kiritsugou", "Jack", "Steve", "Loki", "Gilgamesh", "Zeus", "Blacky", "Mordred", "Romio",
				"Cleus", "Kazu", "Hikki", "Ethan", "Kotomine", "Okeanus", "Phil", "Cris", "Klein", "Plebe", "Rinri", "Lotus", "Atlas"
			};
			
			guerrero gue;
			
			//Por cada espacio disponible
			for (int i = 0; i < max; i++) {
				
				//Nueva instancia de guerrero
				gue = new guerrero();
				
				// Asignar Nombre
				gue.setNom(nomGuerreros[(int)(Math.random() * nomGuerreros.length)] + " " + (i+1));;
				
				// Asignar Clase
				switch ((int)(Math.random() * 6 + 1)) {

			    case 1:
			    	gue.setClase(new archer());
			    	break;
			    case 2:
			        gue.setClase(new assassin());
			        break;
			    case 3:
			        gue.setClase(new berserker());
			        break;
			    case 4:
			        gue.setClase(new lancer());
			        break;
			    case 5:
			        gue.setClase(new rider());
			        break;
			    case 6:
			        gue.setClase(new saber());
			        break;
			}
				
				// A�adir guerrero
				coli.a�adirGuerrero(gue);
			}
			return coli;
		}
		
		//Setters & Getters
		
		public guerrero getGuerrero(int posicion) {
			if(posicion < 0 || posicion > guerreros.length) {
			return null;
			} else {
				return guerreros[posicion];
			}
		}
		
		public guerrero[] getGuerreros() {
			return guerreros;
		}

		public void setGuerreros(guerrero[] guerreros) {
			this.guerreros = guerreros;
		}

		public int getNumGuerreros() {
			return numGuerreros;
		}

		public void setNumGuerreros(int numGuerreros) {
			this.numGuerreros = numGuerreros;
		}

		public int getTopGuerreros() {
			return topGuerreros;
		}

		public void setTopGuerreros(int topGuerreros) {
			this.topGuerreros = topGuerreros;
		}
}
