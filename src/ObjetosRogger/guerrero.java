package ObjetosRogger;

public class guerrero {
	
	private String nom;
	private int asesinatos;
	private guerreroClase clase;
	private guerreroEstado estado;
	private static int contador = 0;
	
	//Constructor
	protected guerrero() {
		nom = "Sense nom";
		asesinatos = 0;
		estado = guerreroEstado.VIVO;
		contador++;
	}
	
	guerrero (String nom, int asesinatos, guerreroEstado estado) {
		this();
		this.nom = nom;
		this.asesinatos = asesinatos;
		this.estado = estado;
	}
	
	public boolean batalla (guerrero enemigo) {
		if (this.clase.fuerteContra(enemigo.clase)) {
			return true; //Si eres fuerte contra el ganas.
		} else {
			return false; //Si no eres fuerte contra el pierdes.
		}
	}
	
	//Metodos
	@Override
	public String toString() {
		return "Nombre: " + nom + clase + clase.arma;
	}
	
	//Getters & Setters
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAsesinatos() {
		return asesinatos;
	}
	public void setAsesinatos(int asesinatos) {
		this.asesinatos = asesinatos;
	}
	public guerreroEstado getEstado() {
		return estado;
	}
	public void setEstado(guerreroEstado estado) {
		this.estado = estado;
	}
	public static int getContador() {
		return contador;
	}
	public static void setContador(int contador) {
		guerrero.contador = contador;
	}
	public guerreroClase getClase() {
		return clase;
	}
	public void setClase(guerreroClase clase) {
		this.clase = clase;
	}
	

}
