package ObjetosRogger;

import ObjetosRogger.clases.*;

public class main {

	public static void main(String[] args) {

		// Generar coliseos
		coliseo coliseo1 = coliseo.generarColiseo(5);
		coliseo coliseo2 = coliseo.generarColiseo(5);
		
					
		System.out.println("\n Coliseo 1");
		coliseo1.visualizar();
		
		System.out.println("\n Coliseo 2");
		coliseo2.visualizar();
		
		System.out.println("\n");
		luchar(coliseo1, coliseo2);
	}
	
	public static String espaciado(String texto, int numEspacios) {
	    return String.format("%-" + numEspacios + "s", texto);
	}
	
	public static void luchar(coliseo coliseo1, coliseo coliseo2) {
		guerrero guerrero1;
		guerrero guerrero2;
		
		for (int i=0;i<coliseo1.getGuerreros().length;i++) {
			guerrero1 = coliseo1.getGuerrero(i);
			guerrero2 = coliseo2.getGuerrero(i);
			
			if (guerrero1 == null || guerrero2 == null) {
				return;
			} else {
				if (guerrero1.batalla(guerrero2)) {
					System.out.println(guerrero1.getNom() + " ha ganado contra " + guerrero2.getNom());
				} else {
					System.out.println(guerrero2.getNom() + " ha ganado contra " + guerrero1.getNom());
				}
			}
		}
	}
}
