package ObjetosRogger;

public abstract class guerreroClase {
	
	protected String nombreClase;
	protected int vida;
	protected int agilidad;
	protected guerreroArma arma;
	
	//Constructor
		
	protected guerreroClase (String clase, int vida, int agilidad) {
		this.nombreClase = clase;
		this.vida = vida;
		this.agilidad = agilidad;
	}
	
	@Override
	public String toString() {
		return "	Clase: " + main.espaciado(nombreClase, 9) + "	Vida: " + vida + "	Agilidad: " + agilidad;
	}
	
	//Setters & Getters
	public String getClaseNombre() {
		return nombreClase;
	}

	public void setClaseNombre(String clase) {
		this.nombreClase = clase;
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getAgilidad() {
		return agilidad;
	}

	public void setAgilidad(int agilidad) {
		this.agilidad = agilidad;
	}

	public String getNombreClase() {
		return nombreClase;
	}

	public void setNombreClase(String nombreClase) {
		this.nombreClase = nombreClase;
	}

	public guerreroArma getArma() {
		return arma;
	}

	public void setArma(guerreroArma arma) {
		this.arma = arma;
	}

	public abstract boolean fuerteContra(guerreroClase contrincante);
}
