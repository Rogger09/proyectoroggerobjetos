package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.daga;
import ObjetosRogger.armas.arco;

public class assassin extends guerreroClase {
	
	//Constructor
	public assassin () {
		super("Assassin", 35, 30);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new daga();
		} else {
			this.arma = new arco();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof lancer || contrincante instanceof archer) {
			return true;
		}
		return false;
	}
}
