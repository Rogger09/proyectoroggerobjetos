package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.escudoEspada;
import ObjetosRogger.armas.espada;

public class rider extends guerreroClase {
	
	//Constructor
	public rider () {
		super("Rider", 25, 40);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new escudoEspada();
		} else {
			this.arma = new espada();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof saber || contrincante instanceof lancer || contrincante instanceof assassin) {
			return true;
		}
		return false;
	}
}
