package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.lanza;
import ObjetosRogger.armas.espada;

public class lancer extends guerreroClase {
	
	//Constructor
	public lancer () {
		super("Lancer", 40, 20);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new lanza();
		} else {
			this.arma = new espada();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof saber || contrincante instanceof archer) {
			return true;
		}
		return false;
	}
}
