package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.espada;
import ObjetosRogger.armas.mandoble;

public class saber extends guerreroClase {
	
	//Constructor
	public saber () {
		super("Saber", 50, 15);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new espada();
		} else {
			this.arma = new mandoble();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof berserker || contrincante instanceof assassin || contrincante instanceof archer) {
			return true;
		}
		return false;
	}
}
