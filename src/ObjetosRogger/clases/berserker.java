package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.mandoble;
import ObjetosRogger.armas.escudoEspada;

public class berserker extends guerreroClase {
	
	//Constructor
	public berserker () {
		super("Berserker", 40, 10);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new mandoble();
		} else {
			this.arma = new escudoEspada();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof assassin || contrincante instanceof rider || contrincante instanceof lancer) {
			return true;
		}
		return false;
	}
}
