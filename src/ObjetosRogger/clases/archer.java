package ObjetosRogger.clases;

import ObjetosRogger.*;
import ObjetosRogger.armas.arco;
import ObjetosRogger.armas.daga;

public class archer extends guerreroClase {
	
	//Constructor
	public archer () {
		super("Archer", 45, 25);
		
		if ((int)(Math.random() * 2 + 1) == 1) {
			this.arma = new arco();
		} else {
			this.arma = new daga();
		}
	}
	
	@Override
	
	public boolean fuerteContra (guerreroClase contrincante) {
		if (contrincante instanceof berserker || contrincante instanceof rider) {
			return true;
		}
		return false;
	}
}
